# Politechnika Lodzka PBL MidTermPresentation

Tak tak tak

## Git project

### Uses

Edit only the `public/index.html`.

For pechakuta, the autoslides is set to 20, so we only have to defing images as background for each slides and that's it !!

### See the slides working

## How to create slides

### Slides

The presentation markup hierarchy needs to be `.reveal > .slides > section` where the `section` represents one slide and can be repeated indefinitely. If you place multiple `section` elements inside of another `section` they will be shown as vertical slides. The first of the vertical slides is the "root" of the others (at the top), and will be included in the horizontal sequence. For example:

```html
<div class="reveal">
	<div class="slides">
		<section>Single Horizontal Slide</section>
		<section>
			<section>Vertical Slide 1</section>
			<section>Vertical Slide 2</section>
		</section>
	</div>
</div>
```

### Add images, change color ...

Simple use html and css. 
See [w3School](https://www.w3schools.com/) or if you need help ask me.

For images, add into `public/media/` folder.

## Reveal project

### Git

https://github.com/hakimel/reveal.js

### License

MIT licensed

Copyright (C) 2018 Hakim El Hattab, [http://hakim.se](http://hakim.se)

Edited by Damien Auvray, [https://damienauvray.fr](https://damienauvray.fr)